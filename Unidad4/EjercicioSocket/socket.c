#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/socket.h>

unsigned char MAC[6], MASK[4], IP[4];

void obtenerDatos(int ds) //ds = Descriptor de socket
{
	int i = 0;
	char nombre[10];
	struct ifreq nic; //man netdevice
	printf("\nIntroduce la interfaz de red: ");
	scanf("%s", nombre);
	strcpy(nic.ifr_name, nombre);

	//Obtener el indice SIOCGIFINDEX
	if (ioctl(ds, SIOCGIFINDEX, &nic)==-1)
	{
		perror("Error al obtener indice");
	}else{
		printf("\nEl indice es: %d\n", nic.ifr_ifindex);
	}
	
	//Obtener MAC SIOCGIFHWADDR
	if (ioctl(ds, SIOCGIFHWADDR, &nic)==-1)
	{
		perror("Error al obtener la MAC");
	}else{
		memcpy(MAC, nic.ifr_hwaddr.sa_data, 6);		
		printf("\nMAC: ");
		for(i=0; i<6; i++){
			printf("%2x:", MAC[i]);
		}
	}
	
	//Obtener IP SIOCGIFADDR
	if (ioctl(ds, SIOCGIFADDR, &nic)==-1)
	{
		perror("Error al obtener la IP");
	}else{
		memcpy(MAC, nic.ifr_addr.sa_data, 6);		
		printf("\nIP: ");
		for(i=2; i < 6; i++){
			printf("%2d:", IP[i]);
		}
	}

}
int main(){
	int packet_socket;

	packet_socket = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

	if(packet_socket == -1){

		perror("Error al abrir socket\n");
		exit(1);
	}else{
		perror("Exito al abrir socket\n");
		obtenerDatos(packet_socket);
	}
	close(packet_socket);
	printf("\n");
}
	

