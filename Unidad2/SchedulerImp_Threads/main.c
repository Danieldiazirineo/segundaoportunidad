#include "scheduler.h"

void *reproducir_musica(void *params);
void *descargando_archivo(void *params);
void *proseso_general(void *params);

int main(int argc, char const *argv[]){
	task *t1 = crear_Task(1, UN_SEGUNDO, "Tarea_1", "Reproducir Música", ACTIVO, reproducir_musica);
	task *t2 = crear_Task(1, DOS_SEGUNDOS, "Tarea_2", "Abrir Youtube", NO_ACTIVO, proseso_general);
	task *t3 = crear_Task(3, TRES_SEGUNDOS, "Tarea_3", "Escribir Texto en Word", ACTIVO, proseso_general);
	
	array_task *array = crear_ArrayTasks();

	agregar_Task(array, *t1);
	agregar_Task(array, *t2);
	agregar_Task(array, *t3);
	

	ejecutar_Tasks(array);

	free(array);
	free(t1);
	free(t2);
	free(t3);
	return 0;
}

void *reproducir_musica(void *params){
	char *proceso_realizar;
	proceso_realizar = (char *)params;
	printf("\t EJECUTANDO PROCESO: [%s]\n", proceso_realizar);
	pthread_exit(NULL);
}

void *descargando_archivo(void *params){
	char *proceso_realizar;
	proceso_realizar = (char *)params;
	printf("\tEJECUTANDO PROCESO: [%s]\n", proceso_realizar);
	pthread_exit(NULL);
}

void *proseso_general(void *params){
	char *proceso_realizar;
	proceso_realizar = (char *)params;
	printf("\tEJECUTANDO PROCESO: [%s]\n", proceso_realizar);
	pthread_exit(NULL);
}
